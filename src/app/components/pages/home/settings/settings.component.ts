import { Component, OnInit } from '@angular/core';
import { typeDocument } from './typeDocument';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../../../../models/user';
import { Profile } from '../../../../models/profile';
import { UserService } from '../../../../services/user.service';
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  formulario: FormGroup;
  public types = typeDocument;
  user:User;

  constructor(private userService:UserService) {
    this.user = this.userService.user;
    this.user.profile = this.user.profile?this.user.profile:new Profile;

    this.formulario = new FormGroup({
      'name': new FormControl(this.user.name, [Validators.required,
      Validators.minLength(3)]),
      'email': new FormControl(this.user.email, [
      Validators.pattern("[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}")]),
      'email_confirmation': new FormControl(this.user.email, [
      Validators.pattern("[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{1,5}")]),
      'type_document': new FormControl(this.user.profile.type_document, [Validators.required]),
      'nit': new FormControl(this.user.profile.nit, [Validators.required,
      Validators.minLength(3)]),
      'photo': new FormControl(this.user.profile.photo),
      'phone': new FormControl(this.user.profile.phone, [Validators.required,
      Validators.minLength(3)]),
      'address': new FormControl(this.user.profile.address, [Validators.required,
      Validators.minLength(3)])
    });
  }

  ngOnInit() {
  }

  updateProfie(){
    this.userService.updateProfile(this.formulario.value)
    .subscribe(
      data=>console.log(data)
    );
  }
}
