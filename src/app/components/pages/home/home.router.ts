import { Routes } from '@angular/router';
import { SettingsComponent } from './settings/settings.component';
import {CategoryComponent} from './category/category.component';
import {ProductComponent} from './product/product.component';


export const Home_ROUTES: Routes = [
    {path:'category',component:CategoryComponent},
    {path:'product',component:ProductComponent},
    {path:'settings',component:SettingsComponent},
    { path: '**', pathMatch: 'full', redirectTo: 'category' }
];
