import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../../services/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formulario:FormGroup;

  constructor(private auth:AuthService,
    private router:Router) { 
      
    this.formulario = new FormGroup({
      'email': new FormControl('',[Validators.required,Validators.email]),
      'password': new FormControl('',[Validators.required,Validators.minLength(3)]),
      'remember': new FormControl()
    });

    if(localStorage.getItem("email")){
      this.formulario.get('email').setValue(localStorage.getItem("email"));
      this.formulario.get('remember').setValue(true);
    }
  }

  ngOnInit() {
  }

  loading(){
    Swal.fire({
      allowOutsideClick:false,
      type:'info',
      text:'Espere por favor...'
    });
    Swal.showLoading();
  }

  login(){
    this.loading();
    this.auth.login(this.formulario.value).subscribe(resp =>{
      Swal.close();
      if(this.formulario.get('remember').value){
        localStorage.setItem("email",this.formulario.controls.email.value);
      }
      this.router.navigateByUrl('home');
    }, err =>{
      Swal.fire({
        type:'error',
        title:'Error al autenticar',
        footer:this.auth.showErrors(err)
      });
    });
  }

  socialLogin(provider:string){
    this.loading();
    this.auth.loginFirebase(provider).then(
      user=>{
        Swal.close();
        this.router.navigateByUrl('home');
      }
    ).catch(
      err=>{
        console.log("error de social login");
        console.log(err);
        Swal.fire({
          type:'error',
          title:`Error al autenticar con ${provider}`,
          footer:this.auth.showErrors(err)
        });
      }
    );
  }

}
