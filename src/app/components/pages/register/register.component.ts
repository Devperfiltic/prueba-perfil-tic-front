import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  formulario:FormGroup;
  constructor(private auth:AuthService,
    private router:Router) { 
    this.formulario = new FormGroup({
      'name': new FormControl('',[Validators.required]),
      'email': new FormControl('',[Validators.required,Validators.email]),
      'email_confirmation': new FormControl('',[Validators.required,Validators.email]),
      'password': new FormControl('',[Validators.required,Validators.minLength(3)]),
      'password_confirmation': new FormControl('',[Validators.required,Validators.minLength(3)]),
      'confirmation': new FormControl(false,[Validators.requiredTrue])
    });
  }

  ngOnInit() {
  }

  register(){

    Swal.fire({
      allowOutsideClick:false,
      type:'info',
      text:'Espere por favor...'
    });

    Swal.showLoading();
    
    this.auth.register(this.formulario.value).subscribe(user=>{
      Swal.close();
      this.router.navigateByUrl('home');
    }, err =>{
      Swal.fire({
        type:'error',
        title:'Error al autenticar',
        footer:this.auth.showErrors(err)
      });
    });
  }

}
