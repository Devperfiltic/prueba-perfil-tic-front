import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { User } from '../models/user';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiUrl = environment.apiUrl;
  user:User;
  constructor(private http: HttpClient) { 
    this.user  = JSON.parse(localStorage.getItem('user'));
  }

  getHeaders(){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${localStorage.getItem('token')}`,
      'Accept':'application/json'
    });

    return headers;
  }

  resendEmail(){
    return this.http.get(`${this.apiUrl}/profile/verify-email`,{headers:this.getHeaders()});
  }

  getUser() {
    return this.http.get(`${this.apiUrl}/profile/user`, { headers: this.getHeaders() })
    .toPromise().then(
      (data:any)=>{
        this.refresUSer(data);
        return this.user;
      }
    ).catch(
      err => null
    );
  }

  updateProfile(datos:any){
    return this.http.put(`${this.apiUrl}/profile/update`,datos,
    { headers: this.getHeaders() })
    .pipe(map(
      (data:any)=>{
        this.refresUSer(data);
        return data;
      }
    ));
  }

  refresUSer(data){
    localStorage.setItem('user', JSON.stringify(data.user));
    this.user = data.user;
  }
}
