export class Profile {
    type_document:string='cc';
    nit:string='';
    photo:string='';
    phone:string='';
    address:string='';
    user_id:string='';
    created_at:string='';
    updated_at:string='';
}