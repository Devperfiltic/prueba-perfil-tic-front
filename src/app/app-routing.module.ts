import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/pages/home/home.component';
import { LoginComponent } from './components/pages/login/login.component';
import { RegisterComponent } from './components/pages/register/register.component';
import { Home_ROUTES } from './components/pages/home/home.router';
import { AuthGuard } from './guards/auth.guard';
import { NoAuthGuard } from './guards/no-auth.guard';
import { VerifyUserGuard } from './guards/verify-user.guard';
import { VerifyUserComponent } from './components/pages/verify-user/verify-user.component';
import { WellcomeComponent } from './components/pages/wellcome/wellcome.component';

const routes: Routes = [
  {path:'home',component:HomeComponent,children:Home_ROUTES,canActivate: [AuthGuard,VerifyUserGuard] },
  {path:'login',component:LoginComponent,canActivate: [NoAuthGuard]},
  {path:'register',component:RegisterComponent,canActivate: [NoAuthGuard]},
  {path:'wellcome',component:WellcomeComponent},
  {path:'verify',component:VerifyUserComponent,canActivate: [AuthGuard]},
  {path:'**',pathMatch:'full',redirectTo:'wellcome'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
